FROM devkitpro/devkita64

ARG uid
ARG branch=develop
ENV branch=$branch

RUN apt-get update && \
    apt-get install -y unzip \
    git \
    cmake \
    make \
    bsdmainutils \
    elfutils \
    binutils \
    zlib1g-dev \
    gcc g++ && \
    dkp-pacman -Syu --noconfirm && \
    dkp-pacman -S --noconfirm devkitARM devkitPPC libogc libctru 3ds-dev wii-dev gamecube-dev ppc-zlib ppc-libpng && \
    useradd -d /developer -m developer && \
    chown -R developer:developer /developer && \
    rm -rf /var/lib/apt/lists/*

ADD cmake_toolchain_files /opt/cmake_toolchain_files

ENV HOME=/developer
ENV DEVKITPRO=/opt/devkitpro
ENV DEVKITARM=/opt/devkitpro/devkitARM
ENV DEVKITPPC=/opt/devkitpro/devkitPPC
ENV CTRULIB=/opt/devkitpro/libctru
ENV CMAKE_TOOLCHAIN_FILE_DIR=/opt/cmake_toolchain_files

USER developer
WORKDIR /developer
VOLUME /developer

CMD /bin/bash
